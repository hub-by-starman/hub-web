# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.http import HttpRequest
from channels import Group
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Desired_Temp, Heater_State, Heat_Schedule, Default_Rank, New_User_Register_Ability, AI_State
from sensorWorker.models import measured_temperature, Average, relay_status
from login_handling.models import Profile
import json
from django.http import JsonResponse
from django.contrib.auth.models import Group, User
import datetime
import time
import RPi.GPIO as GPIO
from django.contrib.auth.decorators import user_passes_test
import datetime
import paho.mqtt.publish as publish
from django.contrib.auth.decorators import login_required
from sensorWorker.management.commands.control_heater import Command
from wifi import Cell, Scheme
import subprocess


@login_required(login_url='/')
def admin_page(request):
    current_user = request.user
    new_user_register_ability = New_User_Register_Ability.objects.values_list('new_user_register_ability', flat=True)[0]
    default_rank = Default_Rank.objects.values_list('default_rank', flat=True)[0]
    ai_state = AI_State.objects.values_list('ai_state', flat=True)[0]
    user_group = User.objects.all()
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

    return render(
        request,
        'admin.html',
        context={'user_group':user_group, 'default_rank':default_rank, 'new_user_register_ability':new_user_register_ability, 'ai_state':ai_state, 'theme_color':theme_color, 'accent_color':accent_color},
    )

@login_required(login_url='/')
def ranks(request):
    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    return render(
        request,
        'ranks.html',
        context={'theme_color':theme_color, 'accent_color':accent_color},
    )


@login_required(login_url='/')
def main_base(request):
    current_user = request.user
    server_time = int(round(time.time() * 1000))
    heater_state=Heater_State.objects.values_list('heater_state', flat=True).order_by('-id')[0]
    desired_temp=Desired_Temp.objects.values_list('desired_temp', flat=True)[0]
    current_temp=measured_temperature.objects.values_list('measured_temp', flat=True).order_by('-id')[0]
    average=Average.objects.values_list('average', flat=True)[0]
    schedule_hours = list(Heat_Schedule.objects.values_list('state_at_hour', flat=True))
    ai_state = AI_State.objects.values_list('ai_state', flat=True)[0]
    relayState = relay_status.objects.values_list('status', flat=True)[0]
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    print (accent_color)
    return render(
        request,
        'main.html',
        context={'heater_state':heater_state, 'current_temp':current_temp, 'desired_temp':desired_temp, 'average':average, 'server_time':server_time, 'schedule_hours': schedule_hours, 'ai_state':ai_state, 'theme_color':theme_color, 'accent_color':accent_color, 'relay_state':relayState},
    )

@login_required(login_url='/')
def heat_schedule(request):
    current_user = request.user
    member_group = Group.objects.get(name="Member").user_set.all()
    user_group = Group.objects.get(name="User").user_set.all()
    schdule_hours = list(Heat_Schedule.objects.values_list('state_at_hour', flat=True))
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    return render(
        request,
        'schedule.html',
        context={'schdule_hours':schdule_hours, 'member_group':member_group, 'user_group':user_group, 'theme_color':theme_color, 'accent_color':accent_color},
    )

@login_required(login_url='/')
def user_settings(request):
    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]
    return render(
        request,
        'settings.html',
        context={'theme_color':theme_color, 'accent_color':accent_color},
    )


@login_required(login_url='/')
def heater_graphs(request):

    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

    yesterday = (datetime.datetime.now() - datetime.timedelta(days=1))
    temps=list(measured_temperature.objects.values_list('measured_temp', flat=True).filter(measured_time__range=(yesterday, datetime.datetime.now())))
    temp_times= list(measured_temperature.objects.values_list('measured_time', flat=True).filter(measured_time__range=(yesterday, datetime.datetime.now())))
    temp_times_formatted = []


    for s in temp_times:
        xx = (s.timestamp())*1000
        temp_times_formatted.append(xx)
    return render(
        request,
        'graphs.html',
        context={'theme_color':theme_color, 'accent_color':accent_color, 'temps':temps, 'temp_times_formatted':temp_times_formatted, 'zips': zip(temp_times_formatted, temps)},
    )


@login_required(login_url='/')
def logs(request):
    if request.user.groups.filter(name='Admin').exists():
        this_week = (datetime.datetime.now() - datetime.timedelta(days=7))

        current_user = request.user
        theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
        accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

        heater_states=list(Heater_State.objects.values_list('heater_state', flat=True).filter(heater_set_time__range=(this_week, datetime.datetime.now())))
        heater_times =list(Heater_State.objects.values_list('heater_set_time', flat=True).filter(heater_set_time__range=(this_week, datetime.datetime.now())))
        heater_users =list(Heater_State.objects.values_list('user', flat=True).filter(heater_set_time__range=(this_week, datetime.datetime.now())))


        return render(
            request,
            'logs.html',
            context={'theme_color':theme_color, 'accent_color':accent_color, 'zips': zip(heater_states, heater_times, heater_users)},
        )
    else:
        return None

@login_required(login_url='/')
def light(request):
    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

    return render(
        request,
        'light.html',
        context={'theme_color':theme_color, 'accent_color':accent_color},
    )


@login_required(login_url='/')
def dash(request):
    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

    return render(
        request,
        'dash.html',
        context={'theme_color':theme_color, 'accent_color':accent_color},
    )

@login_required(login_url='/')
def device_search(request):
    current_user = request.user
    theme_color = Profile.objects.filter(user = current_user).values_list('theme_color', flat=True)[0]
    accent_color = Profile.objects.filter(user = current_user).values_list('accent_color', flat=True)[0]

    get_wifi = ((subprocess.check_output( "sudo /home/ace/Project/website/venv/bin/python /home/ace/Project/website/manage.py wifi_scan", shell=True)).decode("utf-8")).splitlines()

    return render(
        request,
        'device_search.html',
        context={'theme_color':theme_color, 'accent_color':accent_color, 'get_wifi':get_wifi},
    )



@login_required(login_url='/')
def heater_change_state(request):
    if request.user.groups.filter(name='Admin').exists() or request.user.groups.filter(name='Member').exists():
        value = request.GET.get('value')
        current_user = request.user

        print ("Recieved value of: "),
        print (value)
        Command.set_heater(Command, value, current_user)

    return HttpResponse(request)

@login_required(login_url='/')
def schedule_hour_toggle(request):
    current_user = request.user

    if current_user.groups.filter(name='Admin').exists() or request.user.groups.filter(name='Member').exists():
        value = request.GET.get('value')
        current_state=Heat_Schedule.objects.filter(id=value).values('state_at_hour').filter(id=value)
        result = [(q['state_at_hour']) for q in current_state]
        result = result[0]
        print( result )
        if result == 1:
            new_schedule_hour = Heat_Schedule.objects.filter(id=value).update(state_at_hour='0')
        else:
            new_schedule_hour = Heat_Schedule.objects.filter(id=value).update(state_at_hour='1')
    return HttpResponse(request)

@login_required(login_url='/')
def desired_temp_change(request):
    current_user = request.user

    if current_user.groups.filter(name='Admin').exists() or request.user.groups.filter(name='Member').exists():
        value = request.GET.get('value')
        print ("Changing desired temp")
        desired_temp_change_object = Desired_Temp.objects.update(desired_temp=value)
        Command.set_heater(Command, "null", current_user)
    return HttpResponse(request)

@login_required(login_url='/')
def permission_level_change(request):
    if request.user.groups.filter(name='Admin').exists():

        userid = request.GET.get('userid')
        group = request.GET.get('new_permission_level')
        old_group = request.GET.get('current_permission_level')
        g = Group.objects.get(name=old_group)
        g.user_set.remove(userid)
        my_group = Group.objects.get(name=group)
        my_group.user_set.add(userid)
    return HttpResponse(request)


@login_required(login_url='/')
def user_delete(request):
    if request.user.groups.filter(name='Admin').exists():
        userid = request.GET.get('userid')
        u = User.objects.get(id = userid)
        u.delete()
    return HttpResponse(request)


@login_required(login_url='/')
def change_default_permission_level(request):
    if request.user.groups.filter(name='Admin').exists():
        value = request.GET.get('default_permission_level')
        change_default_permission_level = Default_Rank.objects.update(default_rank=value)
    return HttpResponse(request)


@login_required(login_url='/')
def toggle_register_ability(request):

    if request.user.groups.filter(name='Admin').exists():
        print ("User is in Admin group")
        value = request.GET.get('new_user_register_ability')
        change_register_ability = New_User_Register_Ability.objects.update(new_user_register_ability=value)
    return HttpResponse(request)


@login_required(login_url='/')
def toggle_ai_state(request):
    if request.user.groups.filter(name='Admin').exists():
        value = request.GET.get('new_ai_state')
        change_ai_state = AI_State.objects.update(ai_state=value)
    return HttpResponse(request)


@login_required(login_url='/')
def change_theme(request):
    theme_color = request.GET.get('theme_color')
    accent_color = request.GET.get('accent_color')
    print (accent_color)
    change_theme =Profile.objects.filter(user=request.user).update(theme_color=theme_color, accent_color=accent_color)
    print("Theme changed")


    return HttpResponse(request)
