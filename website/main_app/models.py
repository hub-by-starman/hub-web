# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models



class Heater_State(models.Model):
    heater_state = models.CharField(max_length=15)
    heater_set_time = models.DateTimeField(editable=False, auto_now_add=True)
    user = models.CharField(max_length=150)
    
class Desired_Temp(models.Model):
    desired_temp = models.DecimalField(default=0, max_digits=5, decimal_places=2)

class Heat_Schedule(models.Model):
    state_at_hour = models.SmallIntegerField(default=0)

class Default_Rank(models.Model):
    default_rank = models.CharField(max_length=6, default="Admin")

class New_User_Register_Ability(models.Model):
    new_user_register_ability = models.CharField(max_length=8, default="Enabled")

class AI_State(models.Model):
    ai_state = models.CharField(max_length=8, default="On")
