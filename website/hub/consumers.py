from channels import Group
import json
from channels.auth import channel_session_user, channel_session_user_from_http





@channel_session_user_from_http
def ws_connect(message):
    if message.user.id is not None:
        Group('users').add(message.reply_channel)
        Group('users').send({
            'text': json.dumps({
                'username': message.user.id,
                'is_logged_in': True
            })
        })


@channel_session_user
def ws_message(message):
    if message.user.id is not None:

        payload = json.loads(message['text'])
        payload['reply_channel'] = message.content['reply_channel']
        Group('users').send({'text': message['text']})


@channel_session_user
def ws_disconnect(message):
    if message.user.id is not None:

        Group('users').send({
            'text': json.dumps({
                'username': message.user.username,
                'is_logged_in': False
            })
        })
        Group('users').discard(message.reply_channel)
