from channels import Group
from django.core.management import BaseCommand
import time
import os
import RPi.GPIO as GPIO
from sensorWorker.models import measured_temperature
import json
from datetime import datetime, timedelta
#The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):

    os.system('modprobe w1-gpio')
    os.system('modprobe w1-therm')



    def get_raw_temp(self):
        temp_sensor = '/sys/bus/w1/devices/28-00000998ac08/w1_slave'
        f = open(temp_sensor, 'r')
        lines = f.readlines()
        f.close()
        return lines


    def get_temp_celcius(self):

    	lines = self.get_raw_temp()
    	while lines[0].strip()[-3:] != 'YES':
    		time.sleep(0.2)
    		lines = get_raw_temp()


    	temp_output = lines[1].find('t=')

    	if temp_output != -1:
    		temp_string = lines[1].strip()[temp_output+2:]
    		temp_c = float(temp_string) / 1000.0


    	return temp_c


    def handle(self, *args, **options):
        x = 0
        temperature1 = 0
        temperature2 = 0
        time = datetime.now() - timedelta(minutes=20)

        while True:


            #Compare current temp measurement with last and only save if different.
            temperature1 = self.get_temp_celcius()
            if (abs(temperature1 - temperature2) > 0.1):
                temperature2 = self.get_temp_celcius()

                Group("users").send({'text': json.dumps({ 'measured_temp': temperature1})})
                now = datetime.now()
                difference = now - time
                if ( difference.total_seconds() > 300 ):

                    time = datetime.now()
                    save_temp = measured_temperature(measured_temp= temperature1)
                    save_temp.save()
