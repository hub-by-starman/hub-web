# -*- coding: utf-8 -*-
#!/usr/bin/env python

from channels import Group
from django.core.management import BaseCommand
from sensorWorker.models import measured_temperature, Average, Active_Times
import paho.mqtt.publish as publish
from main_app.models import Desired_Temp, Heater_State, Heat_Schedule, AI_State
import time
import os
import datetime
from sensorWorker.models import measured_temperature, relay_status
import json
import statistics

#The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):

    def gpio_low(self):
        last_active_start_time = Active_Times.objects.values_list('start_time', flat=True).last()
        last_active_finish_time = Active_Times.objects.values_list('finish_time', flat=True).last()
        now = datetime.datetime.now()

        print ("Last active start time was: "),
        print (last_active_start_time)
        print ("Last active finish time was: "),
        print (last_active_finish_time)

        if last_active_finish_time == None and last_active_start_time != None:
            print("Sending off message to device")
            publish.single("heat_control", payload="off", client_id="raspberry_pi", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
            t = Active_Times.objects.last()
            t.finish_time = now
            t.save()
        else:
            print("Sending off message to device")
            publish.single("heat_control", payload="off", client_id="raspberry_pi", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
        return "success"

    def handle(self, *args, **options):

        while True:
            time.sleep(10)
            self.ai()
            self.set_heater("null", "System")


    def gpio_high(self):

        active_times_list_length = Active_Times.objects.count()
        last_active_start_time = Active_Times.objects.values_list('start_time', flat=True).last()
        last_active_finish_time = Active_Times.objects.values_list('finish_time', flat=True).last()
        now = datetime.datetime.now()

        print ("Last active start time was: "),
        print (last_active_start_time)
        print ("Last active finish time was: "),
        print (last_active_finish_time)
        print ("List of active times has a length of: "),
        print (active_times_list_length)

        if active_times_list_length > 0:

            if last_active_finish_time != None and last_active_start_time != None:
                print("Sending on message to device")

                publish.single("heat_control", payload="on", client_id="raspberry_pi", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
                new_entry = Active_Times(start_time=now)
                new_entry.save()

            elif last_active_finish_time == None and last_active_start_time != None:
                print("Sending on message to device")
                publish.single("heat_control", payload="on", client_id="raspberry_pi", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
            else:
                Active_Times.objects.last().delete()

        else:
            print("Sending on message to device")

            publish.single("heat_control", payload="on", client_id="raspberry_pi", auth={'username':"3CaSgsJTHK6FawfAvbFXyM685rsztJ5AESozRtV82r8c9A9pZ4bkXNSodPM3ET5K9gg8zJFTEMV5GepwGovvTWiezCAmYHsrsMJm67AbQyodsNjK2W5d2M8", 'password':"85tde2SG8X4HqjqTtMVroWihzTvYi7b7BZxmyb3L6bvKDbz2AAL9eAqRcQ7yCf96s5qXTxc27LzKJwuBaUwDMQVba7ecRhereFKwpqetjftjhokqiDV8qie"})
            new_entry = Active_Times(start_time=now)
            new_entry.save()
        return "success"

    def ai(self):
        start_times = list(Active_Times.objects.values_list('start_time', flat=True))
        finish_times = list(Active_Times.objects.values_list('finish_time', flat=True))

        averages = []

        for index, elem in enumerate(finish_times):
            times = list(measured_temperature.objects.filter(measured_time__range=(start_times[index], elem)).values_list('measured_time', flat=True))
            temps = list(measured_temperature.objects.filter(measured_time__range=(start_times[index], elem)).values_list('measured_temp', flat=True))

            if len(temps) > 1:

                first_temp = temps[0]
                last_temp = temps[-1]

                first_time = times[0]
                last_time = times[-1]

                temp_change = last_temp - first_temp
                time_change = last_time - first_time



                if time_change != 0 and temp_change > 0 :
                    average = time_change.total_seconds()/float(temp_change)


                    averages.append(average)


        averages_median = statistics.median(averages)
        print ("Median of averages: "),
        print (averages_median)
        for index, x in enumerate(averages):
            if x < 0.5*averages_median or x > 1.5*averages_median:
                averages.remove(x)


        print ("List of averages is: "),
        print (averages)





        total_averages = sum(averages)
        total_averages = total_averages/len(averages)

        Group("users").send({'text': json.dumps({ 'average': total_averages})})

        t = Average.objects.last()
        t.average = total_averages
        t.save()

    def set_heater(self, value, user):

        relay = relay_status.objects.values_list('status', flat=True)[0]
        print ("Relay is: "),
        print (relay)
        if(relay == "connected"):

            current_temp=measured_temperature.objects.values_list('measured_temp', flat=True).order_by('-id')[0]

            ai_state=AI_State.objects.values_list('ai_state', flat=True)[0]

            desired_temp=Desired_Temp.objects.values_list('desired_temp', flat=True)[0]



            heater_state=Heater_State.objects.values_list('heater_state', flat=True).order_by('-id')[0]

            if value == "null":
                set_heater_state=Heater_State.objects.values_list('heater_state', flat=True).order_by('-id')[0]
            else:
                set_heater_state=value


            print ("Desired temp is: "),
            print ( desired_temp ),
            print ("Last measured temp is: "),
            print ( current_temp ),


            if set_heater_state in ("0", "constant_on_0", "constant_on_1"):

                if current_temp < desired_temp:
                    print ( "Turning device on as heater is set to Constant On and the current temperature is less than desired temperature")
                    Command.gpio_high(Command)
                    new_heater_state = "constant_on_0"
                else:
                    print ( "Turning device off as heater is set to Constant On but the current temperature is more than desired temperature")
                    Command.gpio_low(Command)
                    new_heater_state = "constant_on_1"

            elif set_heater_state in ("1", "use_schedule_0", "use_schedule_1", "use_schedule_2", "use_schedule_3", "use_schedule_4"):

                print ("Heater is set to Use Schedule")

                now = datetime.datetime.now()
                current_time = (((now.weekday()*24)+now.hour ) + 1)
                schedule =  list(Heat_Schedule.objects.values_list('state_at_hour', flat=True))
                temps_list = list(measured_temperature.objects.values_list('measured_temp', flat=True))
                current_schedule_status = Heat_Schedule.objects.filter(id=current_time).values_list('state_at_hour', flat=True)[0]

                print( "Current time is: " ),
                print ( now )
                print ("Current hour is: "),
                print ( current_time )
                print ("Schedule status at current hour is: "),
                print ( current_schedule_status )


                if current_schedule_status == 1:
                    if current_temp < desired_temp:
                        print ( "Turning device on as heater is set to Use Schedule and the user has set schedule to be on at the current time, and current_temp is less than desired temp")
                        Command.gpio_high(Command)
                        new_heater_state = "use_schedule_0"

                    else:
                        print ( "Turning device off as heater is set to Use Schedule and the user has set schedule to be on at the current time, but current_temp is higher than desired temp")
                        Command.gpio_low(Command)
                        new_heater_state = "use_schedule_1"

                elif ai_state == "Enabled":
                    print ( "AI is Enabled" )
                    average=Average.objects.values_list('average', flat=True)[0]
                    print ( "Average time to increase by 1 is: " ),
                    print ( average )



                    x = current_time
                    next_calendar = None

                    for index, elem in enumerate(schedule):
                        if index == x:
                            if elem == (1):
                                print ("Next time turn on is:"),
                                print (index)
                                next_calendar = index
                                x = 300
                            x = x + 1

                    if next_calendar == None:
                        for index, elem in enumerate(schedule):
                            if elem == (1):
                                print ("Next time turn on is:"),
                                print (index)
                                next_calendar = index
                                x = 300
                            x = x + 1


                    if next_calendar > 24 and next_calendar <= 48:
                        next_calendar_hour = next_calendar - 24
                        next_calendar_day = 1
                    elif next_calendar > 48 and next_calendar <= 72:
                        next_calendar_hour = next_calendar - 48
                        next_calendar_day = 2

                    elif next_calendar > 72 and next_calendar <= 96:
                        next_calendar_hour = next_calendar - 72
                        next_calendar_day = 3

                    elif next_calendar > 96 and next_calendar <= 120:
                        next_calendar_hour = next_calendar - 96
                        next_calendar_day = 4

                    elif next_calendar > 120 and next_calendar <= 144:
                        next_calendar_hour = next_calendar - 120
                        next_calendar_day = 5

                    elif next_calendar > 144 and next_calendar <= 168:
                        next_calendar_hour = next_calendar - 144
                        next_calendar_day = 6

                    else:
                        next_calendar_day = 0
                        next_calendar_hour = next_calendar

                    if next_calendar_day != now.weekday():
                        now_next = now + datetime.timedelta(1)
                    else:
                        now_next = now


                    next_time = datetime.datetime(now_next.year, now_next.month, now_next.day, next_calendar_hour, 0, 0, 0)
                    print ("Next time device scheduled to turn on is: "),
                    print (next_time)

                    print ("Current time is: "),
                    print (now)

                    time_to_next_turn_on = next_time - now
                    print ("Time between now and switch on: "),
                    print (time_to_next_turn_on)


                    time_to_hit_desired_temp_estimation = (((float(desired_temp) - float(current_temp))*float(average)))
                    print ("Estimated time to hit desired temp: "),
                    print (time_to_hit_desired_temp_estimation)

                    if time_to_hit_desired_temp_estimation > time_to_next_turn_on.total_seconds():
                        Command.gpio_high(Command)
                        print ("Turning device on as heater is set to Use Schedule and AI is enabled and the system estimates it will reach desired temp by next schduled on time")
                        new_heater_state = "use_schedule_2"
                    else:
                        Command.gpio_low(Command)
                        print ("Turning device off as heater is set to Use Schedule and AI is enabled but the system estimates it will not reach desired temp by next scheduled on time")
                        new_heater_state = "use_schedule_3"
                else:
                    print ( "Turning device off as heater is set to Use Schedule with no AI and the schedule isn't set on at the current time")
                    Command.gpio_low(Command)
                    new_heater_state = "use_schedule_4"

            else:
                print ("Heater is set to Constant Off")
                Command.gpio_low(Command)
                new_heater_state = "constant_off_0"

            print("Original heater state was: "),
            print(heater_state)
            print("New heater state is: "),
            print(new_heater_state)

            if heater_state != new_heater_state:
                print("Setting new heater state")
                save_state = Heater_State(heater_state= new_heater_state, user=user)
                save_state.save()
        else:
            Command.gpio_low(Command)
