# Hub Web


This project is deticated to all files being stored on the main Hub device. 


The Hub device is the heart of the project. It is a raspberry Pi. Currently it has a DS18B20 temperature sensor connected to it and it is stored in a box, however, adding extra sensors modularily is a planned feature.
The Raspberry Pi hosts Raspbian CLI as an OS and a Nginx server runs on it. The Nginx server interfaces with a Django server which runs a website. This website shows two types of connected device. Light or Heater. If a light is connected, it will have a schedule and a graphs page to control when the light turns on and off as well as a user control page to turn the device on and off manually. For the heater, all the same features are offered, but also temperature readings are taken from the DS18B20. On the webpage the user can set their desired temperature and the heater will turn on and off depending on what the current temperature is. An AI also calculates the average times a heater takes to increase in temperature and uses that value to show the user how long until the desired temperature will be reached and also switch on the device early to reach a desired temperature by the time the user scheduled the system to be on.
The hub device connects to the users Wifi and runs an AP network for relay devices to connect to it, allowing the system to continue functioning when the internet goes down. It uses Let's Encrypt for TLS encryption over the web and DuckDNS to host a dynamic DNS based website so the user doesn't need to enter an IP.

The website features a full login system with a permissions system. It also comes with themes and an aesthetically pleasing site.

The project allows full control of any plug-in heater or home-heating that lacks a thermostat. Many renters cannot mess with their thermostat and therefore can not install a competing product which requires integrating with a thermostat. This device is easily plugged in and removed.



# WEBSITE
<b>Login:<b>
![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/login_web.png)


<b>Registration:<b>
![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/registration_web.png)

![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/registration_dark_red_web.png)


<b>Heat Control Page:<b>

A user can control everything about a connected relay device from this page

![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/main_page_web.png)

![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/main_page_dark_green_web.png)

![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/main_page_light_blue_web.png)


<b>Admin Page:<b>(With names censored)

Only the admin can access this page where they have control over other users and settings

![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/security_page_web.png)


<b>Schedule:<b>

Allows the user to set a relay device to switch on and off at certain times of the week
![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/schedule_web.png)


<b>Graph:<b>

Shows a graph of temperature changes connected to a heater device
![alt text](https://gitlab.com/hub-by-starman/hub-meta/raw/master/media/graphs_web.png)
